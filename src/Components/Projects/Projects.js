import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProjectCard from "./ProjectCards";
import Particle from "../Particle";
import { DiJsBadge, DiReact, DiNodejs, DiCss3, DiHtml5 } from "react-icons/di";
import { SiRedux } from "react-icons/si";
import tictac from "../../Assets/Projects/test.png"
import Smatbot from "../../Assets/Projects/159166169-3425cdb9-b9be-4ece-8c78-0cfb804b4765.png";
import todo from "../../Assets/Projects/159935716-d67d8391-514b-43db-8637-1125826704e0.gif";

function Projects() {
	return (
		<Container fluid className="project-section">
			<Particle />
			<Container>
				<h1 className="project-heading">
					My Recent <strong className="purple">Works </strong>
				</h1>
				<p style={{ color: "white" }}>
					Here are a few projects I've worked on recently.
				</p>
				<Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
					<Col md={4} className="project-card">
						<ProjectCard
							imgPath={tictac}
							isBlog={false}
							title="Daily planner"
							description="Created a simple calendar application that allows a user to save events for each hour of the day by modifying starter code. This app will run in the browser and feature dynamically updated HTML and CSS powered by jQuery.."
							techstack="HTML | CSS | JS"
							link="https://musikavanhu.github.io/Dailyplanner/"
							git="https://github.com/Musikavanhu/Dailyplanner"
						/>
					</Col>

					<Col md={4} className="project-card">
						<ProjectCard
							imgPath={Smatbot}
							isBlog={false}
							title="AI Camera"
							description=" Machine learning image recognization for web based applications"
							techstack="HTML | CSS | JS"
							link="https://musikavanhu.github.io/AI-based-Webcam/"
							git="https://github.com/Musikavanhu/AI-based-Webcam"
						/>
					</Col>

					<Col md={4} className="project-card">
						<ProjectCard
							imgPath={todo}
							isBlog={false}
							title="Algorithm Path Finder"
							description="Visual path finder using Dijkstra's algorithm to find the shortest route possible using react framework"
							techstack="HTML | CSS | JS | React"
							link="https://github.com/Musikavanhu/Path-Finder"
							git="https://github.com/Musikavanhu/Path-Finder"
						/>
					</Col>

				
				</Row>
			</Container>
		</Container>
	);
}

export default Projects;
